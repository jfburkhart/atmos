BEGIN;
CREATE TABLE "fpi_fpspecies" (
    "species_name" varchar(50) NOT NULL,
    "species_id" integer NOT NULL PRIMARY KEY,
    "half_life" real,
    "wet_depositionA" real,
    "wet_depositionB" real,
    "dry_deposition_gases" real,
    "dry_deposition_henrys" real,
    "dry_deposition_f0" real,
    "dry_deposition_rho" real,
    "dry_deposition_dquer" real,
    "dry_deposition_dsig" real,
    "dry_deposition_veloc" real,
    "molweight" real,
    "oh_reaction" real,
    "number_assoc" real,
    "koa" real
)
;
CREATE TABLE "fpi_fprun_tracers" (
    "id" integer NOT NULL PRIMARY KEY,
    "fprun_id" integer NOT NULL,
    "fptracer_id" integer NOT NULL,
    UNIQUE ("fprun_id", "fptracer_id")
)
;
CREATE TABLE "fpi_fprun_in_projects" (
    "id" integer NOT NULL PRIMARY KEY,
    "fprun_id" integer NOT NULL,
    "fpproject_id" integer NOT NULL,
    UNIQUE ("fprun_id", "fpproject_id")
)
;
CREATE TABLE "fpi_fprun" (
    "id" integer NOT NULL PRIMARY KEY,
    "run_name" varchar(200) NOT NULL,
    "run_dir" varchar(200) NOT NULL,
    "run_description" text NOT NULL
)
;
CREATE TABLE "fpi_fptracer" (
    "id" integer NOT NULL PRIMARY KEY,
    "run_id" integer NOT NULL REFERENCES "fpi_fprun" ("id"),
    "species_id" integer NOT NULL REFERENCES "fpi_fpspecies" ("species_id"),
    "tracer_n" integer,
    UNIQUE ("run_id", "species_id")
)
;
CREATE TABLE "fpi_fpproject_runs" (
    "id" integer NOT NULL PRIMARY KEY,
    "fpproject_id" integer NOT NULL,
    "fprun_id" integer NOT NULL REFERENCES "fpi_fprun" ("id"),
    UNIQUE ("fpproject_id", "fprun_id")
)
;
CREATE TABLE "fpi_fpproject" (
    "id" integer NOT NULL PRIMARY KEY,
    "created_by_id" integer,
    "contact" varchar(75),
    "status" varchar(20) NOT NULL,
    "project_name" varchar(50) NOT NULL,
    "product_directory" varchar(255) NOT NULL,
    "release_directory" varchar(255),
    "project_description" text NOT NULL,
    "created_on" datetime NOT NULL,
    "updated_on" datetime NOT NULL,
    "slug" varchar(50) NOT NULL,
    "notes" text,
    "url" varchar(200)
)
;
CREATE INDEX "fpi_fptracer_bc73c538" ON "fpi_fptracer" ("run_id");
CREATE INDEX "fpi_fptracer_836f7483" ON "fpi_fptracer" ("species_id");
CREATE INDEX "fpi_fpproject_b5de30be" ON "fpi_fpproject" ("created_by_id");
CREATE INDEX "fpi_fpproject_a951d5d6" ON "fpi_fpproject" ("slug");
COMMIT;
