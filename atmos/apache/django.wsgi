#! /usr/bin/env python
import sys
import os
import site

# Fix for 'illegal' print statements
sys.stdout = sys.stderr

root = os.path.join(os.path.dirname(__file__), '..')
sys.path.insert(0, root)

path = ['/xnilu_wrk/flex_wrk/WEB_APS/atp/','/xnilu_wrk/jfb/hg', '/xnilu_wrk/flex_wrk/bin64/site-packages']

for p in path:
    if p not in sys.path:
        site.addsitedir(p)

os.environ['DJANGO_SETTINGS_MODULE'] = 'atmos.settings'
os.environ['MPLCONFIGDIR'] = '/tmp'


import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
