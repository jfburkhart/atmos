from django import forms

class ContactForm(forms.Form):
    subject = forms.CharField()
    email = forms.EmailField(required=False)
    message = forms.CharField(widget=forms.Textarea)


class InteractivePlotForm(forms.Form):
    
    def __init__(self, proj_set=None,
                 header=None,
                 *args, **kwargs):
            
        super(InteractivePlotForm, self).__init__(*args, **kwargs)
        
        
        if proj_set is not None:
            ## Which projects / runs are available
            self.fields['project'].choices = [(d.slug, d) for d in proj_set['projects']]
            self.initial['project'] = proj_set['projslug']
            
            self.fields['run'].choices = [(d.slug, d) for d in proj_set['runs']]
            self.initial['run'] = proj_set['runslug']
            
            # Hidden Elements
            self.initial['projectID'] = proj_set['projslug']
            self.initial['run_dir'] = proj_set['run_dir']
            
            
        
        if header is not None:
            # Widgets generated from Header Input
            
            ## Initialize Lat / Lon Boxes
            self.fields['llcrnrlat'].initial = header.latitude[1]
            self.fields['llcrnrlon'].initial = header.longitude[1]
            self.fields['urcrnrlat'].initial = header.latitude[-2]
            self.fields['urcrnrlon'].initial = header.longitude[-2]
            self.fields['l_lat'].initial = header.latitude[1]
            self.fields['l_lon'].initial = header.longitude[1]
            self.fields['r_lat'].initial = header.latitude[-2]
            self.fields['r_lon'].initial = header.longitude[-2]
            
            
            ## Initialize Date Select Fields
            self.fields['p_start'].choices = [(d, d) for d in header.available_dates_dt]
            self.initial['p_start'] = self.fields['p_start'].choices[-1][1]
            self.fields['p_end'].choices = [(d, d) for d in header.available_dates_dt]
            
            ## Ageclass
            if header.direction == 'forward':
                choices = [(0, 'all')]
            else:
                choices = [(i, d) for i, d in enumerate(header.ageclasses.insert(0, 'all'))]
                
            self.fields['ageclass'].choices = choices
    
            ## Which tracer/species to plot
            self.fields['tracer'].choices = [(i, d) for i, d in enumerate(header.species)]
            
            ## Which level
            self.fields['level'].choices= \
                            [(0,'Total Column'), (1, 'Footprint')] + \
                            [(i+2, d) for i, d in enumerate(header.outheight)]
            

    
            ## Do we display the releases widget?
            if header.numpointspec > 1:
                self.fields['numpointspec'].choices = [(d+1, d+1) for d in xrange(header.numpointspec)]
    
            else:
                self.fields['numpointspec'].choices = [(1,1)]
                self.initial['numpointspec'] = 1
                self.fields['numpointspec'].widget = forms.HiddenInput()
                
                self.has_multiple_nps = False
                
            self.header = header
    
    
    has_multiple_nps = True

    # Generated for proj_set
    run = forms.ChoiceField( widget=forms.Select(attrs={'id':'runs'}))
    project = forms.ChoiceField( widget=forms.Select(attrs={'id':'projects'}))
    
    # Widgets generated from Header Input
    tracer = forms.ChoiceField()
    p_start = forms.ChoiceField()
    p_end = forms.ChoiceField(required=False)
    ageclass = forms.ChoiceField()
    numpointspec = forms.ChoiceField()
    level = forms.ChoiceField()

    # File upload option
    plotfile = forms.FileField( required=False )
    flight_movie = forms.BooleanField(required=False)
    timeseries = forms.BooleanField(required=False)

    # Map Paramters
    llcrnrlat = forms.FloatField(initial=23.0)
    llcrnrlon = forms.FloatField(initial=23.0)
    urcrnrlat = forms.FloatField(initial=23.0)
    urcrnrlon = forms.FloatField(initial=23.0)
    l_lat = forms.FloatField(initial=23.0)
    l_lon = forms.FloatField(initial=23.0)
    r_lat = forms.FloatField(initial=23.0)
    r_lon = forms.FloatField(initial=23.0)

    # Hidden Elements
    run_dir = forms.CharField(required=False, widget=forms.HiddenInput())
    projectID = forms.CharField(required=False, widget=forms.HiddenInput())
        

    # Option Fields
    animate = forms.ChoiceField(choices= \
                                [('single', 'single'), ('movie', 'movie')],
                                required=False)
    plot_type = forms.ChoiceField(choices= \
                                [('both', 'both'), ('horizontal', 'horizontal'), ('vertical', 'vertical')])
    unit = forms.ChoiceField(choices= \
                                [('conc', 'concentration (ug m-3)'), ('pptv', 'mixing ratios (ppbv)')])
    
    max_conc = forms.FloatField(required=False)
    min_conc = forms.FloatField(required=False)
    nest = forms.BooleanField(initial=False, required=False)
    asl = forms.BooleanField(required=False)
    log = forms.BooleanField(initial=True, required=False)
    crossmethod = forms.ChoiceField(choices = \
                                    [('pcolormesh', 'no interp'), ('contourf', 'interp')])




