# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'fpSpecies'
        db.create_table('fpi_fpspecies', (
            ('species_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('species_id', self.gf('django.db.models.fields.IntegerField')(primary_key=True)),
            ('half_life', self.gf('django.db.models.fields.FloatField')(default=-999.8, null=True, blank=True)),
            ('wet_depositionA', self.gf('django.db.models.fields.FloatField')(default=-9.9e-09, null=True, blank=True)),
            ('wet_depositionB', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('dry_deposition_gases', self.gf('django.db.models.fields.FloatField')(default=-9.9, null=True, blank=True)),
            ('dry_deposition_henrys', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('dry_deposition_f0', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('dry_deposition_rho', self.gf('django.db.models.fields.FloatField')(default=-9900000000.0, null=True, blank=True)),
            ('dry_deposition_dquer', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('dry_deposition_dsig', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('dry_deposition_veloc', self.gf('django.db.models.fields.FloatField')(default=-9.99, null=True, blank=True)),
            ('molweight', self.gf('django.db.models.fields.FloatField')(default=28.87, null=True, blank=True)),
            ('oh_reaction', self.gf('django.db.models.fields.FloatField')(default=-9.9e-09, null=True, blank=True)),
            ('number_assoc', self.gf('django.db.models.fields.FloatField')(default=-9, null=True, blank=True)),
            ('koa', self.gf('django.db.models.fields.FloatField')(default=-99.99, null=True, blank=True)),
        ))
        db.send_create_signal('fpi', ['fpSpecies'])

        # Adding model 'fpRun'
        db.create_table('fpi_fprun', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('run_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('run_dir', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('run_description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('fpi', ['fpRun'])

        # Adding M2M table for field in_projects on 'fpRun'
        db.create_table('fpi_fprun_in_projects', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fprun', models.ForeignKey(orm['fpi.fprun'], null=False)),
            ('fpproject', models.ForeignKey(orm['fpi.fpproject'], null=False))
        ))
        db.create_unique('fpi_fprun_in_projects', ['fprun_id', 'fpproject_id'])

        # Adding M2M table for field tracers on 'fpRun'
        db.create_table('fpi_fprun_tracers', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fprun', models.ForeignKey(orm['fpi.fprun'], null=False)),
            ('fptracer', models.ForeignKey(orm['fpi.fptracer'], null=False))
        ))
        db.create_unique('fpi_fprun_tracers', ['fprun_id', 'fptracer_id'])

        # Adding model 'fpTracer'
        db.create_table('fpi_fptracer', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('run', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fpi.fpRun'])),
            ('species', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['fpi.fpSpecies'])),
            ('tracer_n', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('fpi', ['fpTracer'])

        # Adding unique constraint on 'fpTracer', fields ['run', 'species']
        db.create_unique('fpi_fptracer', ['run_id', 'species_id'])

        # Adding model 'fpProject'
        db.create_table('fpi_fpproject', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
            ('contact', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('status', self.gf('django.db.models.fields.CharField')(default='complete', max_length=20)),
            ('project_name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('product_directory', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('release_directory', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('project_description', self.gf('django.db.models.fields.TextField')()),
            ('created_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_on', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50, db_index=True)),
            ('notes', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal('fpi', ['fpProject'])

        # Adding M2M table for field runs on 'fpProject'
        db.create_table('fpi_fpproject_runs', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('fpproject', models.ForeignKey(orm['fpi.fpproject'], null=False)),
            ('fprun', models.ForeignKey(orm['fpi.fprun'], null=False))
        ))
        db.create_unique('fpi_fpproject_runs', ['fpproject_id', 'fprun_id'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'fpTracer', fields ['run', 'species']
        db.delete_unique('fpi_fptracer', ['run_id', 'species_id'])

        # Deleting model 'fpSpecies'
        db.delete_table('fpi_fpspecies')

        # Deleting model 'fpRun'
        db.delete_table('fpi_fprun')

        # Removing M2M table for field in_projects on 'fpRun'
        db.delete_table('fpi_fprun_in_projects')

        # Removing M2M table for field tracers on 'fpRun'
        db.delete_table('fpi_fprun_tracers')

        # Deleting model 'fpTracer'
        db.delete_table('fpi_fptracer')

        # Deleting model 'fpProject'
        db.delete_table('fpi_fpproject')

        # Removing M2M table for field runs on 'fpProject'
        db.delete_table('fpi_fpproject_runs')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'fpi.fpproject': {
            'Meta': {'object_name': 'fpProject'},
            'contact': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'product_directory': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'project_description': ('django.db.models.fields.TextField', [], {}),
            'project_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'release_directory': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'runs': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'runs'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['fpi.fpRun']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'complete'", 'max_length': '20'}),
            'updated_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'fpi.fprun': {
            'Meta': {'object_name': 'fpRun'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_projects': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'in_projects'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['fpi.fpProject']"}),
            'run_description': ('django.db.models.fields.TextField', [], {}),
            'run_dir': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'run_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'tracers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'tracers'", 'null': 'True', 'to': "orm['fpi.fpTracer']"})
        },
        'fpi.fpspecies': {
            'Meta': {'object_name': 'fpSpecies'},
            'dry_deposition_dquer': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'dry_deposition_dsig': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'dry_deposition_f0': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'dry_deposition_gases': ('django.db.models.fields.FloatField', [], {'default': '-9.9', 'null': 'True', 'blank': 'True'}),
            'dry_deposition_henrys': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'dry_deposition_rho': ('django.db.models.fields.FloatField', [], {'default': '-9900000000.0', 'null': 'True', 'blank': 'True'}),
            'dry_deposition_veloc': ('django.db.models.fields.FloatField', [], {'default': '-9.99', 'null': 'True', 'blank': 'True'}),
            'half_life': ('django.db.models.fields.FloatField', [], {'default': '-999.8', 'null': 'True', 'blank': 'True'}),
            'koa': ('django.db.models.fields.FloatField', [], {'default': '-99.99', 'null': 'True', 'blank': 'True'}),
            'molweight': ('django.db.models.fields.FloatField', [], {'default': '28.87', 'null': 'True', 'blank': 'True'}),
            'number_assoc': ('django.db.models.fields.FloatField', [], {'default': '-9', 'null': 'True', 'blank': 'True'}),
            'oh_reaction': ('django.db.models.fields.FloatField', [], {'default': '-9.9e-09', 'null': 'True', 'blank': 'True'}),
            'species_id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'species_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'wet_depositionA': ('django.db.models.fields.FloatField', [], {'default': '-9.9e-09', 'null': 'True', 'blank': 'True'}),
            'wet_depositionB': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'})
        },
        'fpi.fptracer': {
            'Meta': {'unique_together': "(['run', 'species'],)", 'object_name': 'fpTracer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['fpi.fpRun']"}),
            'species': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['fpi.fpSpecies']"}),
            'tracer_n': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['fpi']
