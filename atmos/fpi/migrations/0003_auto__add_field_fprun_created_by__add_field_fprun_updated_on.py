# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'fpRun.created_by'
        db.add_column('fpi_fprun', 'created_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True), keep_default=False)

        # Adding field 'fpRun.updated_on'
        db.add_column('fpi_fprun', 'updated_on', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, default=datetime.date(2011, 10, 29), blank=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'fpRun.created_by'
        db.delete_column('fpi_fprun', 'created_by_id')

        # Deleting field 'fpRun.updated_on'
        db.delete_column('fpi_fprun', 'updated_on')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'fpi.fpproject': {
            'Meta': {'object_name': 'fpProject'},
            'contact': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'created_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'product_directory': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'project_description': ('django.db.models.fields.TextField', [], {}),
            'project_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'release_directory': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'runs': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'runs'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['fpi.fpRun']"}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'db_index': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'complete'", 'max_length': '20'}),
            'updated_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        'fpi.fprun': {
            'Meta': {'object_name': 'fpRun'},
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'in_projects': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'in_projects'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['fpi.fpProject']"}),
            'run_description': ('django.db.models.fields.TextField', [], {}),
            'run_dir': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'run_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'tracers': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'tracers'", 'null': 'True', 'symmetrical': 'False', 'to': "orm['fpi.fpTracer']"}),
            'updated_on': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        },
        'fpi.fpspecies': {
            'Meta': {'object_name': 'fpSpecies'},
            'dry_deposition_dquer': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'dry_deposition_dsig': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'dry_deposition_f0': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'dry_deposition_gases': ('django.db.models.fields.FloatField', [], {'default': '-9.9', 'null': 'True', 'blank': 'True'}),
            'dry_deposition_henrys': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'dry_deposition_rho': ('django.db.models.fields.FloatField', [], {'default': '-9900000000.0', 'null': 'True', 'blank': 'True'}),
            'dry_deposition_veloc': ('django.db.models.fields.FloatField', [], {'default': '-9.99', 'null': 'True', 'blank': 'True'}),
            'half_life': ('django.db.models.fields.FloatField', [], {'default': '-999.8', 'null': 'True', 'blank': 'True'}),
            'koa': ('django.db.models.fields.FloatField', [], {'default': '-99.99', 'null': 'True', 'blank': 'True'}),
            'molweight': ('django.db.models.fields.FloatField', [], {'default': '28.87', 'null': 'True', 'blank': 'True'}),
            'number_assoc': ('django.db.models.fields.FloatField', [], {'default': '-9', 'null': 'True', 'blank': 'True'}),
            'oh_reaction': ('django.db.models.fields.FloatField', [], {'default': '-9.9e-09', 'null': 'True', 'blank': 'True'}),
            'species_id': ('django.db.models.fields.IntegerField', [], {'primary_key': 'True'}),
            'species_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'wet_depositionA': ('django.db.models.fields.FloatField', [], {'default': '-9.9e-09', 'null': 'True', 'blank': 'True'}),
            'wet_depositionB': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'})
        },
        'fpi.fptracer': {
            'Meta': {'unique_together': "(['run', 'species', 'tracer_n'],)", 'object_name': 'fpTracer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'run': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['fpi.fpRun']"}),
            'species': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['fpi.fpSpecies']"}),
            'tracer_n': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['fpi']
