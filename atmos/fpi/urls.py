from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

from models import fpProject
import views

projects = {
        'queryset' : fpProject.objects.all(),
        }

urlpatterns = patterns('django.views.generic.list_detail',
    # Species:
    url(R'^species/(?P<pk>\d+)/$', views.fpSpeciesDetailView.as_view(), name='species-detail'),
    
    # Tracers:
    url(R'^tracer/(?P<pk>\d+)/$', views.fpTracerDetailView.as_view(), name='tracer-detail'),
    
    # Runs
    url(r'^$', 'object_list', projects, name='project-list'),
    
    url(R'^project/(?P<slug>\w+)/$', views.fpProjectDetailView.as_view(), name='project-detail'),
    
    url(R'^run/(?P<pk>\w+)/$', views.fpRunDetailView.as_view(), name='run-detail'),
    #url(R'^(?P<object_id>\d+)/interactive/(?P<tracer_id>\d+)/$', direct_to_template, {'template': 'fpi/fprun_entry.html'}),
    url(R'^interactive/$', views.fpRunInteractive.as_view(), name='fp-interactive'),
    url(R'^interactive/instructions/(?P<slug>\w+)/$', views.fpRunInstructionDetailView.as_view(), name='fp-instructions'),
    url(R'^interactive/(?P<slug>\w+)/(?P<runslug>\S+)/$', views.fpRunInteractive.as_view(), name='fp-interactive'),
    url(R'^interactive/(?P<slug>\w+)/$', views.fpRunInteractive.as_view(), name='fp-interactive'),
   
    url(R'^ajax/$', views.header_ops, name='ops'),
    
    
    url(R'^fpi_view/$', views.fpRunInteractive.as_view(), name='fpi-view'),
    #url(R'^fpi_view/$', views.fpRunInteractive.as_view(), name='fpi-view'),
    
    #url(R'^contact/$', views.contact, name='contact'),
    #url(R'^fpi_view/img/(?P<fmt>\w+)$', views.image, name='img'),
    
    )