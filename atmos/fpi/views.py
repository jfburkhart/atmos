# Create your views here.

import sys, os
import json
from datetime import datetime

sys.path.append('/home/jfb/workspace')
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render_to_response
from django.template.context import RequestContext
from django.views.generic import DetailView, CreateView
from django.http import Http404, HttpResponse, HttpResponseRedirect


from models import fpProject, fpRun, fpTracer, fpSpecies
from forms import InteractivePlotForm
from django.conf import settings

from nilu.pflexpart import pflexpart as pf
from nilu.pflexpart import fpi
np = pf.np

def home(request):
    pass

class fpSpeciesDetailView(DetailView):

    queryset = fpSpecies.objects.all()
    context_object_name = "fpSpecies"
    template_name = 'fpi/fpspecies_detail.html'


class fpTracerDetailView(DetailView):

    queryset = fpTracer.objects.all()
    context_object_name = "fpTracer"
    template_name = 'fpi/fptracer_detail.html'

    def get_context_data(self, **kwargs):

        context = kwargs
        context_object_name = self.get_context_object_name(self.object)

        if context_object_name:
            context[context_object_name] = self.object

        speciesQS = fpSpecies.objects.all()
        spc = speciesQS.filter(**{'species_name': self.object.species}).get()
        context['species'] = spc

        return context


class fpRunDetailView(DetailView):

    queryset = fpRun.objects.all()
    context_object_name = "fpRun"
    template_name = 'fpi/fprun_detail.html'


class fpProjectDetailView(DetailView):

    queryset = fpProject.objects.all()
    context_object_name = "fpProject"
    template_name = 'fpi/fpproject_detail.html'


class fpRunInstructionDetailView(DetailView):

    queryset = fpProject.objects.all()
    context_object_name = "fpProject"
    template_name = 'fpi/fpproject_instructions.html'


class fpRunInteractive(CreateView, DetailView):

    queryset = fpProject.objects.all()
    context_object_name = "fpRunInteractive"
    template_name = 'fpi/fpi_interactive.html'
    settings = settings
    
    
    def get_object(self, queryset=None):
        """
        Returns the object the view is displaying.

        Overrides default get_object from 
        File /home/jfb/Dev_Web/django-trunk/django/views/generic/list_detail.py
        """
        # Try to get object from query set.
        
        
        slug = self.kwargs.get('slug', None)
        runslug = self.kwargs.get('runslug', None)
        
        super(fpRunInteractive, self).get_object(queryset=queryset)
        # Use a custom queryset if provided; this is required for subclasses
        # like DateDetailView
        if queryset is None:
            queryset = self.get_queryset()

        if slug is not None:
            qfiltered = queryset.filter(**{'slug': slug})
            try:
                project = qfiltered.get()
            except ObjectDoesNotExist:
                raise Http404(_(u"No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        else:
            project = queryset[0]

        if runslug is not None:

            qfiltered = project.runs.filter(**{'slug': runslug})
            try:
                self.run = qfiltered.get()
            except ObjectDoesNotExist:
                raise Http404(_(u"No %(verbose_name)s found matching the query") %
                          {'verbose_name': queryset.model._meta.verbose_name})
        else:
            self.run = project.runs.all()[0]
        ## Now Model Run Specifics:
        try:
            run_dir = os.path.join(project.product_directory, self.run.run_dir)
            self.H = pf.Header(run_dir, readp=False)
            self.P = {}
            self.P['run_dir'] = run_dir
            self.P['project'] = project
            self.P['projslug'] = project.slug
            self.P['projects'] = queryset.all()

            self.P['run_name'] = self.run.run_name
            self.P['run'] = self.run
            self.P['runslug'] = self.run.slug
            self.P['runs'] = project.runs.all()


        except:
            raise AttributeError(u"Cannot Open Run Directory For: "
                                 u"{0} with tracer: {1}."\
                                 .format(project.project_name, self.run.run_name))

        self.project = project

        return self



    def get_context_data(self, **kwargs):
        super(fpRunInteractive, self).get_context_data(**kwargs)
        context = kwargs
        context_object_name = self.get_context_object_name(self.object)

        if context_object_name:
            context[context_object_name] = self.object

        if self.request.method != 'POST':
            context['form'] = InteractivePlotForm(proj_set=self.P,
                                                   header=self.H)

            #TODO: Need to check how persistent this is.
            #self.request.session['header'] = self.H
            self.request.session['proj_set'] = self.P
        return context


    def post(self, *arg, **kwargs):
        super(fpRunInteractive, self).post(*arg, **kwargs)

        ## load information from session
        self.P = self.request.session['proj_set']
        
        if self.request.FILES:
            plotfile = self.request.FILES['plotfile']
            # assume json if 'application'
            if 'application' in plotfile.content_type:
                cd = json.load(plotfile)
                self.H = pf.Header( cd['run_dir'], nest=cd['nest'], readp=False)
                
                # also need to reset P
                self.kwargs['slug'] = cd['project']
                self.kwargs['runslug'] = cd['run']
                self.get_object(queryset = self.queryset)
                
                # process form
                form = InteractivePlotForm(header=self.H, proj_set=self.P, data=cd)
                
            else:
                # we'll use plotfile below
                self.H = None
        else:
            plotfile = None
            self.H = None
        
        if self.H is None:     
            run_dir = os.path.join(self.P['project'].product_directory, self.P['run'].run_dir)
        
            if 'nest' in self.request.POST:
                nest = True
            else:
                nest = False
    
            self.H = pf.Header( run_dir, nest=nest, readp=False)
            

            # process form
            form = InteractivePlotForm(header=self.H, proj_set=self.P, data=self.request.POST)
#           form = InteractivePlotForm( data=self.request.POST )

        if form.is_valid():

            cd = form.cleaned_data
#           
            # call the factory plotting routine
            # will return a dictionary that may 
            # added to the context
            
            ## check if a file was provided
            coords = None
            
            if plotfile:
                # assume a flighttrack, using plotfile from above
                if plotfile.content_type == 'text/plain':
                    coords = fpi_process_flighttrack(plotfile)
                    
            fpf = fpi.fpPlotFactory(self.H, cd,
                                    coords=coords,
                                    settings=self.settings)

            cd.update(fpf.process_request())
            cd.update({ 'MEDIA_URL' : settings.MEDIA_URL,
                       'MEDIA_ROOT' : settings.MEDIA_ROOT })
            context = cd

            #if not settings.DEBUG:
            #    del self.request.session['cd']
            #    del self.request.session['header']

            return render_to_response('fpi/fpi_view.html', context)

        else:
            #print form.errors.as_text()
            print 'form is not valid'
            form = dict(form=form)
            return render_to_response('fpi/fpi_form_error.html', form)
            


def header_ops(request):
    
    #if request.is_ajax():
    while True:
        q = request.GET.get('q')
        
        if q is not None:
            callrqst, slug, runslug ,= q.split('/')
            projects = fpProject.objects.all()
            project = projects.filter( **{'slug': slug}).get()
            if runslug == 'none':
                run = project.runs.all()[0]
            else:
                run = project.runs.filter(**{'slug': runslug}).get()
            
            run_dir = os.path.join(project.product_directory, run.run_dir)
            H = pf.Header(run_dir, readp=False)
            
            P = {}
            P['run_dir'] = run_dir
            P['project'] = project
            P['projslug'] = project.slug
            P['projects'] = projects

            P['run_name'] = run.run_name
            P['run'] = run
            P['runslug'] = run.slug
            P['runs'] = project.runs.all()
            
            request.session['proj_set'] = P
            
            form = InteractivePlotForm(proj_set=P,
                                       header=H)
            
            if callrqst == 'header':
                template = 'fpi/fpi_ajax_header.html'
            elif callrqst == 'project':
                template = 'fpi/fpi_ajax_project.html'
            data = {
                'form': form,
            }
            return render_to_response(template, data,
                context_instance=RequestContext(request))


def fpi_process_flighttrack(plotfile):
    """ process a simple standard flight track file """
    
    lines = plotfile.readlines()
    coords = []
    for line in lines:
        vals = line.strip().split(',')
        if len(vals) == 4:
            date = [datetime.strptime(vals[0], '%Y%m%d %H%M%S')]
            xyz = [float(i) for i in vals[1:]]
            vals = date + xyz
            coords.append(vals)
    return np.array(coords)


def image(request, fmt):

    #fmt = request.args[1]
    cd = request.session['cd']
    H = request.session['header']
    fpPlotFactory = fpi.fpPlotFactory(H, cd)

    response = HttpResponse(mimetype="image/png")
    if fmt == 'hz':
        fpPlotFactory.plot_horiz(response)
    if fmt == 'vt':
        fpPlotFactory.plot_curtain(response)
    if fmt == 'id':
        plot_request = fpPlotFactory.plot_horiz()
        print plot_request
        response = plot_request['filename'].format(
                        plot_request['project'],
                        plot_request['nspec_ret'],
                        plot_request['format'],
                        plot_request['t_start'])
        print response

    return response
