from django.db import models

from django.contrib.auth.admin import User
from docutils.core import publish_string

# Create your models here.

RUN_STATUS_CHOICES = (
                      ('init', 'Initialized'),
                      ('complete', 'Completed'),
                      ('forecast', 'Forecast [Ongoing]'),
                      )

class fpSpecies(models.Model):
    
    species_name = models.CharField(max_length=50)
    species_id = models.IntegerField(primary_key=True, help_text="Must match 'SPECIES_XXX' (e.g. _003 would be 3)")
    half_life = models.FloatField( null=True, blank=True, default=-999.8)
    wet_depositionA = models.FloatField( null=True, blank=True, default=-9.9E-09)
    wet_depositionB = models.FloatField( null=True, blank=True)
    dry_deposition_gases = models.FloatField( null=True, blank=True, default=-9.9)
    dry_deposition_henrys = models.FloatField( null=True, blank=True)
    dry_deposition_f0 = models.FloatField( null=True, blank=True)
    dry_deposition_rho = models.FloatField( null=True, blank=True, default=-9.9E09)
    dry_deposition_dquer = models.FloatField( null=True, blank=True)
    dry_deposition_dsig = models.FloatField( null=True, blank=True)
    dry_deposition_veloc = models.FloatField( null=True, blank=True, default=-9.99)
    molweight = models.FloatField( null=True, blank=True, default=28.87)
    oh_reaction = models.FloatField( null=True, blank=True, default=-9.9E-09)
    number_assoc = models.FloatField( null=True, blank=True, default=-9)
    koa = models.FloatField( null=True, blank=True, default=-99.99)
    
    def species_num(self):
        return str(self.species_id).zfill(3)
    
    # Temporal Fields
    
    # WeekDay Fields
    
    
    
    
    
    def __unicode__(self):
        return self.species_name



class fpRun(models.Model):
    created_by = models.ForeignKey(User, null=True, blank=True)
    updated_on = models.DateTimeField('date updated', auto_now=True)
    run_name = models.CharField(max_length=200, help_text="Tagline (e.g. 'North American'")
    run_dir = models.CharField(max_length=200, help_text="Directory where 'header' is located")
    in_projects = models.ManyToManyField('fpProject', related_name='in_projects', null=True, blank=True)
    tracers = models.ManyToManyField('fpTracer', related_name='tracers', null=True, blank=True)
    run_description = models.TextField(help_text="Detailed description of Run setup and Tracers. (Displayed in web page).")
    slug = models.SlugField( help_text="what is seen in the URL")
    
    def __unicode__(self):
        return self.run_name



class fpTracer(models.Model):
    
    run = models.ForeignKey(fpRun)
    species = models.ForeignKey(fpSpecies)
    tracer_n = models.IntegerField(null=True, blank=True,
                                   help_text="Tracer number (e.g. _001 would be 1)")
    in_runs = models.ManyToManyField('fpRun', related_name='in_runs', null=True, blank=True)
    
    class Meta:
        unique_together = ['run', 'species', 'tracer_n']
        
    def tracer_num(self):
        return str(self.tracer_n).zfill(3)
    
    def name(self):
        return self.__unicode__()
    
    def __unicode__(self):
        return '{2}:{0}:{1}'.format(self.run, self.species, self.tracer_n)



class fpProject(models.Model):
    
    created_by = models.ForeignKey(User, null=True, blank=True)
    contact = models.EmailField(null=True, blank=True)
    status = models.CharField(max_length=20, choices=RUN_STATUS_CHOICES, default='complete')
    project_name = models.CharField(max_length=50)
    product_directory = models.CharField(max_length=255,
                                         help_text="Location of subdirectories where FLEXPART output is saved.")
    release_directory = models.CharField(max_length=255, null=True, blank=True,
                                        help_text="Optional Releases directory")
    project_description = models.TextField(help_text="Description of Project (not Tracer details).")
    created_on = models.DateTimeField('date created', auto_now_add=True)
    updated_on = models.DateTimeField('date updated', auto_now=True)
    slug = models.SlugField( help_text="what is seen in the URL")
    runs = models.ManyToManyField('fpRun', related_name='runs', null=True, blank=True)
    notes = models.TextField(null=True, blank=True,
                             help_text="Optional related information that may be relavant to display about the project.")
    url = models.URLField(null=True, blank=True)
    
    def instructions(self):
        return publish_string(self.notes, writer_name='html')
    
    def information(self):
        return "{0}:\n {1}".format(self.project_name, self.url)
    
    def __unicode__(self):
        return self.project_name




    

    

