
from django.contrib import admin
from models import fpProject, fpRun, fpTracer, fpSpecies

    
class TracerInline(admin.TabularInline):
    model = fpTracer
    extra = 2

class fpProjectAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_on'
    list_filter = ('status','created_by',)
    list_display = ('project_name', 'created_by', 'information',)
    search_fields = ['project_description']
    prepopulated_fields = {"slug": ('project_name',)}
    #inlines = [RunInline]
    
class fpRunAdmin(admin.ModelAdmin):
    list_filter = ('run_name','in_projects',)
    list_display = ('run_name', )
    prepopulated_fields = {"slug": ('run_name',)}
    #inlines = [TracerInline]
    
class fpTracerAdmin(admin.ModelAdmin):
    list_filter = ('run', 'species')
    list_display = ('run', 'species', 'tracer_num',)
    search_fields = ['tracer_n', 'species', 'run']
    
    
class fpSpeciesAdmin(admin.ModelAdmin):
    list_filter = ('species_name',)
    list_display = ('species_name', 'species_id')
    search_fields = ['species_name', 'species_id']
    fieldsets = [
        (None,             {'fields': ['species_name',
                                       'species_id',
                                       'half_life']}),
        ('Wet Deposition', {'fields': ['wet_depositionA',
                                       'wet_depositionB'],
                            'classes': ['collapse']}),
        ('Dry Deposition', {'fields': [
                                       'dry_deposition_gases',
                                       'dry_deposition_henrys',
                                       'dry_deposition_f0',
                                       'dry_deposition_rho',
                                       'dry_deposition_dquer',
                                       'dry_deposition_dsig',
                                       'dry_deposition_veloc'],
                            'classes': ['collapse']}),
        ('Other Information', {'fields': [
                                       'molweight',
                                       'oh_reaction',
                                       'number_assoc',
                                       'koa'],
                             'classes': ['collapse']}),
    ]
    

    
admin.site.register(fpProject, fpProjectAdmin)
admin.site.register(fpRun, fpRunAdmin)
admin.site.register(fpTracer, fpTracerAdmin)
admin.site.register(fpSpecies, fpSpeciesAdmin)