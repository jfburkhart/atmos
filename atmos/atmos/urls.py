from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^fpi/', include('fpi.urls')),
    #url(r'^$', 'atmos.views.home', name='home'),
    # url(r'^atmos/', include('atmos.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls), name='admin-view'),
     
)


from django.conf import settings

if settings.DEBUG:
    urlpatterns += patterns('',
                           (r'^media/(?P<path>.*)$', 'django.views.static.serve',
                            {'document_root': settings.MEDIA_ROOT}),
                           )