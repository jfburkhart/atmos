

var map, vlayer, controls, bin, fpi_form

function init_OL(){
	var extent = new OpenLayers.Bounds(-180, -90, 180, 90);

	map = new OpenLayers.Map( 'map' );

	var wms = new OpenLayers.Layer.WMS( "OpenLayers WMS",
	         "http://vmap0.tiles.osgeo.org/wms/vmap0?", {layers: 'basic'} );
	
	var renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
	renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

	vlayer = new OpenLayers.Layer.Vector("Vector Layer", {
	                 renderers: renderer
	});

	map.addControl(new OpenLayers.Control.MousePosition());
	map.addLayers([wms, vlayer]);
	map.setCenter(extent, 1);
	map.setOptions({restrictedExtent: extent});


	polyOptions = {sides: 4,
	               irregular: true,
	               size: parseFloat(''),
	               };

	controls = {
	             line: new OpenLayers.Control.DrawFeature(vlayer,
					 		OpenLayers.Handler.Path, {
								handlerOptions : {'maxVertices': 2},
					 			'featureAdded': notice}),

	             polygon: new OpenLayers.Control.DrawFeature(vlayer,
	                                  OpenLayers.Handler.RegularPolygon,
	                                  {handlerOptions: polyOptions,
	               					  'featureAdded': notice}),

	             drag: new OpenLayers.Control.DragFeature(vlayer,
			   						  {'onComplete': notice})
	};

	for(var key in controls) {
	    control = controls[key];
	    map.addControl(control);
	};


	//map.layers[1].onFeatureInsert = function(feature) { 
	//    notice(feature)
	//};
	

	//fpi_form = document.getElementById('fpi_form');
	fpi_form = document.forms[0];

	document.getElementById('navtoggle').checked = true;
	setFromInput()

	//vlayer.events.register('beforefeatureadded', this, before_feature_added);
	//vlayer.events.register('afterfeatureadded', this, after_feature_added);
	//vlayer.events.register('featuremodified', this, feature_modified);
	//vlayer.onFeatureInsert = on_feature_insert;

}

function before_feature_added(feature) {
};

function on_feature_insert(feature){
};

function after_feature_added(feature) {
	console.log('after adding a feature!', feature); 
};

function feature_modified(feature)
{
	console.log('feature modified');
};

function toggleControl(element) {
	for(key in controls) {
	    var control = controls[key];
	        if(element.value == key && element.checked) {
	            control.activate();
	        } else {
	            control.deactivate();
			};
	};
};


function notice(feature) {
	console.log('feature added, running notice');    
	newFeature = feature;
	for (f in vlayer.features)
	{   
	   curFeature = vlayer.features[f];
	   console.log('testing', f, feature.geometry.CLASS_NAME, curFeature.geometry.CLASS_NAME);
	   if (feature.geometry.CLASS_NAME == curFeature.geometry.CLASS_NAME)
	   {
	    	console.log('curFeature will be destroyed: ', f, curFeature);
		   	vlayer.removeFeatures([curFeature]);
	   }
	}
	vlayer.addFeatures([newFeature]);
	setInputValues(newFeature);
};

function setFromInput()
{ 
	y1 = parseFloat(fpi_form.l_lat.value);
	x1 = parseFloat(fpi_form.l_lon.value);
	y2 = parseFloat(fpi_form.r_lat.value);
	x2 = parseFloat(fpi_form.r_lon.value);
	lspoints = new Array();
	lspoints[0] = new OpenLayers.Geometry.Point(x1, y1);
	lspoints[1] = new OpenLayers.Geometry.Point(x2, y2);
	var linestring = new OpenLayers.Feature.Vector( 
	              new OpenLayers.Geometry.LineString(lspoints)
	);
	// console.log('Adding linestring from input: ', linestring);
	vlayer.addFeatures([linestring]);
	notice(linestring);

	y1 = parseFloat(fpi_form.llcrnrlat.value);
	x1 = parseFloat(fpi_form.llcrnrlon.value);
	y2 = parseFloat(fpi_form.urcrnrlat.value);
	x2 = parseFloat(fpi_form.urcrnrlon.value);
	var poly = new OpenLayers.Bounds(x1, y1, x2, y2).toGeometry();
	vector = new OpenLayers.Feature.Vector(poly);
	// console.log('Adding polygon from input: ', vector);
	vlayer.addFeatures([vector]);
	notice(vector);
	                   
};

function setInputValues(feature)
{
	getBounds(feature);
	console.log('feature Class: ', feature.geometry.CLASS_NAME);
	if(feature.geometry.CLASS_NAME == 'OpenLayers.Geometry.LineString')
	{
		setLineInput()
	} else if (feature.geometry.CLASS_NAME == 'OpenLayers.Geometry.Polygon')
	{
		setPolyInput()
	};
};

function getBounds(feature)
{
	this.l = feature.geometry.bounds.left;
	this.b = feature.geometry.bounds.bottom;
	this.r = feature.geometry.bounds.right;
	this.t = feature.geometry.bounds.top;
	this.vert = feature.geometry.getVertices();
};

function setLineInput()
{
	if (this.vert[0].x < this.vert[1].x){
		xl = this.vert[0].x
		yl = this.vert[0].y
		xr = this.vert[1].x
		yr = this.vert[1].y
	} else {
		xl = this.vert[1].x
		yl = this.vert[1].y
		xr = this.vert[0].x
		yr = this.vert[0].y
	}
	fpi_form.l_lon.value = this.xl
	fpi_form.l_lat.value = this.yl;
	fpi_form.r_lon.value = this.xr;
	fpi_form.r_lat.value = this.yr;

};

function setPolyInput()
{
	fpi_form.llcrnrlon.value = this.l;
	fpi_form.llcrnrlat.value = this.b;
	fpi_form.urcrnrlon.value = this.r;
	fpi_form.urcrnrlat.value = this.t;
};

function getProjection()
{
	document.forms['projection'] = map.getProjection()
	document.forms['extent'] = map.getExtent()
};


function resetRectCoords()
{
	fpi_form.llcrnrlon.value = -179;
	fpi_form.llcrnrlat.value = 0;
	fpi_form.urcrnrlon.value = 181;
	fpi_form.urcrnrlat.value = 90;
	vlayer.destroyFeatures();
};

function resetLineCoords()
{
	fpi_form.l_lon.value = -100;
	fpi_form.l_lat.value = 35;
	fpi_form.r_lon.value = 10;
	fpi_form.r_lat.value = 60;
	vlayer.destroyFeatures();
};

function resetCoords()
{
	var now = new Date();
	resetLineCoords();
	resetRectCoords();
	setFromInput();
};

function resetDates()
{
	var now = new Date();
	fpi_form.p_year.value = now.getYear();
	fpi_form.p_month.value = now.getMonth();
	fpi_form.p_day.value = now.getDate();
	fpi_form.p_hour.value = 12;
	fpi_form.e_year.value = now.getYear();
	fpi_form.e_month.value = now.getMonth();
	fpi_form.e_day.value = now.getDay();
	fpi_form.e_hour.value = 12;
};


